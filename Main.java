import java.util.ArrayList;
import com.google.gson.GsonBuilder;

public class Main{
	
	public static ArrayList<Blockchain> Block = new ArrayList<Blockchain>();
	public static int difficulty = 5;

	public static void main(String[] args) {	
		//add our blocks to the blockchain ArrayList:
		
		System.out.println("Trying to Mine block 1... ");
		addBlock(new Blockchain("Hi im the first block", "0"));
		
		System.out.println("Trying to Mine block 2... ");
		addBlock(new Blockchain("Yo im the second block",Block.get(Block.size()-1).hash));
		
		System.out.println("Trying to Mine block 3... ");
		addBlock(new Blockchain("Hey im the third block",Block.get(Block.size()-1).hash));	
		
		System.out.println("\nBlockchain is Valid: " + isChainValid());
		
		String blockchainJson = SHA256.getJson(Block);
		System.out.println("\nThe block chain: ");
	}
	
	public static Boolean isChainValid() {
		Blockchain currentBlock; 
		Blockchain previousBlock;
		String hashTarget = new String(new char[difficulty]).replace('\0', '0');
		
		//loop through blockchain to check hashes:
		for(int i=1; i < Block.size(); i++) {
			currentBlock = Block.get(i);
			previousBlock = Block.get(i-1);
			//compare registered hash and calculated hash:
			if(!currentBlock.hash.equals(currentBlock.calculateHash()) ){
				System.out.println("Current Hashes not equal");			
				return false;
			}
			//compare previous hash and registered previous hash
			if(!previousBlock.hash.equals(currentBlock.previousHash) ) {
				System.out.println("Previous Hashes not equal");
				return false;
			}
			//check if hash is solved
			if(!currentBlock.hash.substring( 0, difficulty).equals(hashTarget)) {
				System.out.println("This block hasn't been mined");
				return false;
			}
			
		}
		return true;
	}
	
	public static void addBlock(Blockchain newBlock) {
		newBlock.mineBlock(difficulty);
		Block.add(newBlock);
	}
}